# Modified IX-USB-CAN drivers

## Install or update

```sh
sudo make dkmsinstall
```


## Update source

- Retrieve new version from https://www.ixxat.com/docs/librariesprovider8/ixxat-english-new/pc-can-interfaces/linux-drivers/socketcan-linux.tgz
- Within the download, get `ix_usb_can_...tgz`.
- Patch the `kernel` folder with the new version.
- Try `make all`
- Fix compiler errors.
