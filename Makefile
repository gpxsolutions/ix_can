mod-name += ix_usb_can

MOD_DIR  := kernel/drivers/net/can/usb/ixxat_usb
KDIR     := /lib/modules/$(shell uname -r)/build
SRC_DIR  := $(shell pwd)/$(MOD_DIR)
DEST_DIR := /lib/modules/$(shell uname -r)/$(MOD_DIR)

.PHONY: all clean install uninstall

all:
	make -C $(KDIR) M=$(SRC_DIR) CONFIG_CAN_IXXAT_USB=m modules

clean:
	make -C $(KDIR) M=$(SRC_DIR) CONFIG_CAN_IXXAT_USB=m clean

install:
	make -C $(KDIR) M=$(SRC_DIR) CONFIG_CAN_IXXAT_USB=m modules_install

# Package version and name from dkms.conf
VER := $(shell sed -n 's/^PACKAGE_VERSION=\([^\n]*\)/\1/p' dkms.conf 2>&1 /dev/null)
MODULE_NAME := $(shell sed -n 's/^PACKAGE_NAME=\([^\n]*\)/\1/p' dkms.conf 2>&1 /dev/null)

dkmsinstall:
	cp -R . /usr/src/$(MODULE_NAME)-$(VER)
	dkms install -m $(MODULE_NAME) -v $(VER)

dkmsremove:
	dkms remove -m $(MODULE_NAME) -v $(VER) --all
	rm -rf /usr/src/$(MODULE_NAME)-$(VER)
